#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static struct block_header *get_block_header(void *data);

bool test_one(){ // Обычное выделение памяти
    printf("Тест 1 начат - Обычное выделение памяти.\n");

    void* block = _malloc(600);
	if (!block) {
        printf("FAILED - память не выделена.\n");
        return false;
    }

  	printf("Тест 1 завершён - память успешно выделена.\n");
  	_free(block);
    return true;
}

bool test_two(){ // Освобождение одного блока из нескольких выделенных
    printf("Тест 2 начат - Освобождение одного блока из нескольких выделенных.\n");

	void* block1 = _malloc(150);
	void* block2 = _malloc(150);
  	void* block3 = _malloc(150);

    if (!block1 || !block2 || !block3){
        printf("FAILED - пустой блок памяти.\n");
        return false;
    }

    _free(block1);
    if (!get_block_header(block1)->is_free){
        printf("FAILED - блок памяти не освобождён.\n");
        return false;
    }

    _free(block2);
	_free(block3);
    printf("Тест 2 завершён - один из нескольких блоков успешно освобождён.\n");
	return true;
}

bool test_three(){ // Освобождение двух блоков из нескольких выделенных
    printf("Тест 3 начат - Освобождение двух блоков из нескольких выделенных.\n");

    void* block1 = _malloc(150);
	void* block2 = _malloc(150);
  	void* block3 = _malloc(150);
    void* block4 = _malloc(150);

    if (!block1 || !block2 || !block3 || !block4){
        printf("FAILED - пустой блок памяти.\n");
        return false;
    }

    _free(block1);
	_free(block2);
    if (!get_block_header(block1)->is_free || !get_block_header(block2)->is_free){
        printf("FAILED - блоки памяти не освобождены.\n");
        return false;
    }

    _free(block3);
	_free(block4);
    printf("Тест 3 завершён - несколько блоков успешно освобождены.\n");
	return true;
}

bool test_four(){ // Память закончилась, новый регион памяти расширяет старый
    printf("Тест 4 начат - Память закончилась, новый регион памяти расширяет старый.\n");

	void* block = _malloc(10500);
    if (!block) printf("FAILED - память не выделена.");

	if (block != ((int8_t*)HEAP_START) + offsetof(struct block_header, contents)) {
		printf("FAILED - регион памяти не увеличился.");
		return false;
	}

    _free(block);
	printf("Тест 4 завершён - новый регион памяти успешно расширил старый.\n");
	return true;

}

bool test_five(){ // Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
    printf("Тест 5 начат - Память закончилась, выделение памяти в другом месте.\n");
	
    mmap(0, 300000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
	void* block = _malloc(45000);
    if (!block) printf("FAILED - память не выделена.");
    
	if (block == (int8_t*)HEAP_START + offsetof(struct block_header, contents)) {
        printf("FAILED - новый регион выделен за счёт старого.");
		return false;
	}

	_free(block);
	printf("Тест 5 завершён - новый регион памяти успешно выделен в другом месте.\n");
	return true;
}

static struct block_header *get_block_header(void *data) {
    return (struct block_header*)((uint8_t*)data - offsetof(struct block_header, contents));
}

int main(){
    if (test_one() && test_two() && test_three() && test_four() && test_five()) printf("Все тесты пройдены успешно!");
    else printf("Один или более тестов не были выполнены успешно.");

    return 0;
}
